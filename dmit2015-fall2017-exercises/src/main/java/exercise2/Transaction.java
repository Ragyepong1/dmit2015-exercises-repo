package exercise2;

import java.util.Date;

public class Transaction {
	private Date date = new Date();
	private char type;
	private double amount;
	private double balance;
	private String desciption;
	
	public Transaction() {

	}

	public Transaction(char type, double amount, double balance, String desciption) {
		this.type = type;
		this.amount = amount;
		this.balance = balance;
		this.desciption = desciption;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getDesciption() {
		return desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
