package exercise2;

import java.util.ArrayList;
import java.util.Date;

public class Account {
	private int id; 
	private double balance, annualInterestRate;	
	private Date dateCreated = new Date();	
	private String name;
	private ArrayList<Transaction> transactions = new ArrayList<>();
	
	// The default constructor
	public Account() {
		
	}
	
	// The constructor with input fields
	public Account(int id, double balance, String name) {
		this.id = id;
		this.balance = balance;
		this.name = name;
	}
	
	
	// Getters and setters
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	
	public double getMonthlyInterestRate() {
		return annualInterestRate / 12.0 / 100.0;
	}
	
	public double getMonthlyInterest() {
		return getMonthlyInterestRate() * getBalance();
	}
	
	public void withdraw(double amount, String description) {
		if(amount > 0) {
			balance -= amount;
			Transaction transactionObject = new Transaction('W', amount, balance, description);
			transactions.add(transactionObject);
		}
	}
	
	public void deposit(double amount, String description) {
		if(amount > 0) {
			balance += amount;
			Transaction transactionObject = new Transaction('D', amount, balance, description);
			transactions.add(transactionObject);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}
	
	
}
