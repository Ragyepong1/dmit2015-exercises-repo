package exercise2;

import static org.junit.Assert.*;

import org.junit.Test;

public class AccountTest {

	@Test
	public void testAll() {
		Account accountObject = new Account(1122, 100, "Richard");
		accountObject.setAnnualInterestRate(1.5);
		accountObject.deposit(30, "From Mom");
		accountObject.deposit(40, "From imaginary girlfriend");
		accountObject.deposit(50, "From imaginary ex-girlfriend who still owes you money");
		accountObject.withdraw(5, "Red Cross Donation");
		accountObject.withdraw(4, "RedBull");
		accountObject.withdraw(3, "PSN Indie title");
		
		// Print the ID, balance, annuralInterestRate, dateCreated for the current account
		// Using the System.out.println("") statement
		
		System.out.println("Account ID: " + accountObject.getId());
		System.out.println("Balance: " + accountObject.getBalance());
		System.out.println("Annual Interest Rate: " + accountObject.getAnnualInterestRate());
		System.out.println("Date Created: " + accountObject.getDateCreated());
		System.out.println("Account Name: " + accountObject.getName());
		
		// Print all the transactions
		
		// Check to see if your name is Richard
		 assertEquals("Richard", accountObject.getName());
		// Make sure the interest rate is 1.5
		 assertEquals(1.5, accountObject.getAnnualInterestRate(), 0.0);
		// Make sure the balance is... 
		 assertEquals(208, accountObject.getBalance(), 0.0);
		// Make sure the number of Transactions is 6
		 assertEquals(6, accountObject.getTransactions().size(), 0);
		// Check all 3 withdraws
		// Check all 3 deposits
	}

}
