package exercise1;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class AccountTest {

	@Test
	public void testAll() {
		Account accountObject = new Account(1122, 20000);
		accountObject.setAnnualInterestRate(4.5);
		accountObject.withdraw(2500);
		accountObject.deposit(3000);
		
		// Test that the balance in 19500
		assertEquals(20500, accountObject.getBalance(), 0);
		
		// Check that the monthly interest is $73.12
		assertEquals(76.88, accountObject.getMonthlyInterest(), 0.005);
		
		// Make sure that the dateCreated is today
		Date today = new Date();
		assertEquals(0, accountObject.getDateCreated().compareTo(today));
		
	}

}
