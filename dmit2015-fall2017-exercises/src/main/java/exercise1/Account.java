package exercise1;

import java.util.Date;

public class Account {
	private int id; 
	private double balance, annualInterestRate;	
	private Date dateCreated = new Date();	
	
	// The default constructor
	public Account() {
		
	}
	
	// The constructor with input fields
	public Account(int id, double balance) {
		this.id = id;
		this.balance = balance;
	}
	
	
	// Getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	
	public double getMonthlyInterestRate() {
		return annualInterestRate / 12.0 / 100.0;
	}
	
	public double getMonthlyInterest() {
		return getMonthlyInterestRate() * getBalance();
	}
	
	public void withdraw(double amount) {
		if(amount > 0)
			balance -= amount;
	}
	
	public void deposit(double amount) {
		if(amount > 0)
			balance += amount;
	}
}
